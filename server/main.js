//Load the libs
const q = require("q");
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const express = require("express");

const pool = mysql.createPool({
	host: "localhost", port: 3306,
	user: "user", password: "user",
	database: "grocery",
	connectionLimit: 4
});

const mkQuery = function(sql, pool) {
	return (function() {
		const defer = q.defer();
		//Collect arguments
		const args = [];
		for (var i in arguments)
			args.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}

			conn.query(sql, args, function(err, result) {
				if (err) 
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});

		return (defer.promise);
	});
}

//Create an instance of the application
app = express();

app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));
app.use(express.static(path.join(__dirname, "../client")));

//SELECT upc12, brand, name FROM grocery_list WHERE brand LIKE %$param1% OR name LIKE %$param1%;
const SELECT_GROCERYLIST_DEFAULT_BRAND_A = "select upc12, brand, name from grocery_list order by brand ASC limit 20";
const SELECT_GROCERYLIST_DEFAULT_BRAND_D = "select upc12, brand, name from grocery_list order by brand DESC limit 20";
const SELECT_GROCERYLIST_DEFAULT_NAME_A = "select upc12, brand, name from grocery_list order by name ASC limit 20";
const SELECT_GROCERYLIST_DEFAULT_NAME_D = "select upc12, brand, name from grocery_list order by name DESC limit 20";

const SELECT_GROCERYLIST_DEFAULT = "select upc12, brand, name from grocery_list limit 20";
const SELECT_GROCERYLIST_SEARCH = "select upc12, brand, name from grocery_list where brand like ? or name like ? limit 20";
const UPDATE_GROCERYLIST = "UPDATE grocery_list SET brand = ? , name = ? WHERE upc12 = ? ";
const INSERT_GROCERYLIST = 
"insert into grocery_list (upc12, brand, name) values (?,?,?)";

const getDefaultlist = mkQuery(SELECT_GROCERYLIST_DEFAULT,pool);
const insertGrocery = mkQuery(INSERT_GROCERYLIST,pool);
//const updateGroceryData = mkQuery(UPDATE_GROCERYLIST,pool);
/*
switch(expression) {
    case n:
        code block
        break;
    case n:
        code block
        break;
    default:
        code block
}
*/

app.get("/getdefault", function(req, resp){
	console.log ("In app.post, get default list");
	
	getDefaultlist()
		.then(function(result){
			resp.status(200);
			resp.type("application/json");
			resp.json(result);
		}).catch(function(err){
			resp.status(404);
			resp.end();
		})
})

app.get("/getInfo/:term", function(req,resp){
	const searchTerm = "%" + req.params.term + "%";
	var searchResult = "";
	console.log (">>> In server app. get :search >>> %s", searchTerm);
	//Get a connection from the pool
    //Error or a connection object
    pool.getConnection(function(err, conn) {
        //Check if the pool has returned an error
        if (err) {
            console.error("Error: ", err);
            handleError(err, resp);
            return;
		}
		
		conn.query(SELECT_GROCERYLIST_SEARCH, [searchTerm, searchTerm], 
				function(err, result){
			//Check if the query has resulted in an error
            if (err) {
                console.error(">>>> select: ", err);
                resp.status(500);
                resp.end(JSON.stringify(err));

                conn.release();
                return;
			}
			searchResult = result;
			//console.log(">>>>Search result >>>>");
			//console.log(searchResult);
			resp.status(200);
			resp.type("application/json")
			resp.json(searchResult);
		})
		conn.release();
		return;
	})
});

app.put("/editData", function(req,resp){
	var upcid = req.body.upcid;
	var brand = req.body.brand;
	var name = req.body.name;
	console.log(">>>>app.put, UPCID = %d", upcid);
	console.log("name, %s", name);
	console.log("brand, %s", brand);

	pool.getConnection(function(err, conn) {
        //Check if the pool has returned an error
        if (err) {
            console.error("Error: ", err);
            handleError(err, resp);
            return;
		}
		//var testSQL = "UPDATE grocery_list SET brand = 'Riceland111' , name = 'Riceland American Jazmine Rice2' WHERE upc12 = 35200264013";
		//conn.query(testSQL, 
		conn.query(UPDATE_GROCERYLIST, [brand, name, upcid], 
		
				function(err, result){
			//Check if the query has resulted in an error
            if (err) {
                console.error(">>>> Update: ", err);
                resp.status(500);
                resp.end(JSON.stringify(err));

                conn.release();
                return;
			}
			//Update sucessfully
			
			resp.status(200);
			resp.type("text/html")
			resp.send("Saved!");
		})
		conn.release();
		return;
	})

})

app.post("/insertData", function(req, resp){
	//"insert into grocery_list (upc12, brand, name) values (?,?,?)";
	//upcid: upcid, brand:brand, name:productname
	var upc12 = req.body.upcid;
	var brand = req.body.brand;
	var name = req.body.name;
	insertGrocery(upc12,brand,name)
		.then(function(result){
			resp.status(200);
			resp.type("text/html")
			resp.send("Inserted!");
		}).catch(function(err){
			resp.status(500);
			resp.end();
		})
})
const port = process.env.APP_PORT || 3000;

app.listen(port, function() {
	console.log("Application started at %s on port %d"
			, new Date(), port);
});