(function (){
    var GroceryApp = angular.module("GroceryApp", ["ui.router"]);

    var GroceryConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/views/main.html",
                controller: "GroceryCtrl as groceryCtrl"

            })
			.state("edit", {
                url: "/edit",
                params: {
                    upcid: 0,
                    brand: "",
                    productname: ""
                },
				templateUrl: "/views/edit.html",
				controller: "EditCtrl as editCtrl"

			})
			.state("add", {
				url: "/add",
				templateUrl: "/views/add.html",
				controller: "AddCtrl as addCtrl"
			});

		$urlRouterProvider.otherwise("/main");
	}
	GroceryConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];

    var GrocerySvc = function($http, $q){
        var grocerySvc = this;

        grocerySvc.getDefaultList = function(){
            var defer = $q.defer();
            console.log ("In grocery Service get default list");
            $http.get("/getdefault")
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function(err){
                    defer.reject(err);
                });
            return (defer.promise);
        } //grocerySvc.getDefaultList

        grocerySvc.getInfo = function(searchTerm){
            var defer = $q.defer();
            console.log ("In grocery Service >>> search for %s", searchTerm);
            $http.get("/getInfo/" + searchTerm)
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function(err){
                    defer.reject(err);
                });
            return (defer.promise);
        } //grocerySvc.getProductInfo

        grocerySvc.saveData = function(brand, productname, upcid){
            var defer = $q.defer();
            console.log ("In grocerySvc.saveData >>> edit for %d", upcid);
            $http.put("/editData", { brand:brand, name:productname, upcid: upcid })
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function(err){
                    defer.reject(err);
                });
            return (defer.promise);
        } //grocerySvc.saveData

        grocerySvc.insertData = function(upcid,brand,productname){
            var defer = $q.defer();
            console.log ("In grocerySvc.insertData >>> edit for %d", upcid);
            $http.post("/insertData", {  upcid: upcid, brand:brand, name:productname })
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function(err){
                    defer.reject(err);
                });
            return (defer.promise);
        } //grocerySvc.insertData

    } // GrocerySvc
    GrocerySvc.$inject = [ "$http", "$q" ];

    var GroceryCtrl = function($state, GrocerySvc) {
        var groceryCtrl = this;
        groceryCtrl.displaylist = "";
        groceryCtrl.searchTerm = "";

        groceryCtrl.initMain = function(){
            console.log(">>>Get Default");
            GrocerySvc.getDefaultList()
                .then(function(result){
                    console.log("result>>>");
                    console.log(result);
                    groceryCtrl.displaylist = result;
                }).catch(function(err){
                    console.error("Grocery Controller, Init >>> ", err);
                })
            
        }// groceryCtrl.getDefaultList

        groceryCtrl.search = function(){
            groceryCtrl.displaylist ="";
            console.log(">>>> in groceryCtrl.search >>>");
            console.log(">>>> search for %s", groceryCtrl.searchTerm);
            GrocerySvc.getInfo(groceryCtrl.searchTerm)
                .then(function(result){
                    console.log("result>>>");
                    console.log(result);
                    groceryCtrl.displaylist = result;
                }).catch(function(err){
                    console.error("Grocery Controller >>> ", err);
                })

        } //groceryCtrl.search

        groceryCtrl.addProduct = function() {
            console.log(">>>>Clicked add product>>>>");
            $state.go("add");
        } //groceryCtrl.addProduct

        groceryCtrl.editProduct = function(id,brand,productname) {
            console.log(">>>>Clicked edit , %d", id);
            
            $state.go("edit", { upcid: id, brand: brand, productname: productname  });
        } //groceryCtrl.editProduct

        groceryCtrl.initMain();
    } //GroceryCtrl
    GroceryCtrl.$inject = [ "$state", "GrocerySvc"];

    var EditCtrl = function($stateParams, GrocerySvc){
        var editCtrl = this;
        editCtrl.upcid = $stateParams.upcid;
        editCtrl.brand = $stateParams.brand;
        editCtrl.productname = $stateParams.productname;
        editCtrl.saveFlag = false;
        //console.log(">>>UPCID>>> " + editCtrl.upcid);

        editCtrl.saveData = function(brand,productname){
            
            GrocerySvc.saveData(brand, productname, editCtrl.upcid)
            .then(function(result){
                console.log("Save sucessfully , result>>>");
                console.log(result);
                editCtrl.saveFlag = true;
                
            }).catch(function(err){
                console.error("Edit Controller >>> ", err);
                editCtrl.saveFlag = false;
            })
        }
    }
    EditCtrl.$inject = [ "$stateParams", "GrocerySvc"];

    var AddCtrl = function(GrocerySvc){
        var addCtrl = this;
        addCtrl.saveFlag = false;

        //console.log(">>>UPCID>>> " + editCtrl.upcid);

        addCtrl.insertData = function(){
            
            console.log(">>>>> Add Ctrl >>>");
            GrocerySvc.insertData(addCtrl.upcid,addCtrl.brand,addCtrl.name)
            .then(function(result){
                console.log("Insert sucessfully , result>>>");
                console.log(result);
                addCtrl.saveFlag = true;
                
            }).catch(function(err){
                console.error("Edit Controller >>> ", err);
                addCtrl.saveFlag = false;
            })
        }
    }
    AddCtrl.$inject = [ "GrocerySvc"];
    GroceryApp.config(GroceryConfig);

    GroceryApp.service("GrocerySvc", GrocerySvc);
    
    GroceryApp.controller("EditCtrl", EditCtrl);
    GroceryApp.controller("AddCtrl", AddCtrl);
	GroceryApp.controller("GroceryCtrl", GroceryCtrl);

})();